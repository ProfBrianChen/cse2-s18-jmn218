//Jason Nerreau
//CSE 2: Professor Brian Chen
//hw03 Objective: Practice in writing code that enables the user to input data and gives practice in performing artihmetic operations
//hw03 Task: Compute a specified number of acres affected by hurricane and a specified number of rain inches and from this data, give an output of rain in cubic miles
//Date: 2/11/17


import java.util.Scanner;
//Import the Scanner function to be used within this HW Assignment

public class Convert{
  //Main method to be used with every program within java
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in );
    //Telling the Scanner that you are creating an instance that will take input from STDIN
    
    System.out.print("Enter the affected area in acres: ");
    //Telling user to type in the acreage of the affected area
    
    double acreTotal = myScanner.nextDouble();
    //Accept user input
    
    System.out.print("Enter the rainfall in the affected area: " );
    //Telling user to type in the height of the pyramid
    
    double inchesRainfall = myScanner.nextDouble();
    //Accept user input
    
    double squareMilesAffected;
    //Creating a double variable for that will be the total affected area in square miles
    
    squareMilesAffected = acreTotal/640;
    //Converting the acres affected to the squareMilesAffected based on the conversion factor of 640 acres=1sq.mile
    
    double milesRainfall;
    //Creating a double variable for the rainfall in affected area in milesRainfall
    
    milesRainfall = inchesRainfall/63360;
    //Converting the acres affected to the squareMilesAffected based on the conversion factor of 63360 inches=1 milesRainfall
    
    double cubicMilesRainfall;
    //Creating a double variable which will be the total cubic mileage of rainfall upon the affected area
    
    cubicMilesRainfall = squareMilesAffected * milesRainfall;
    //Calculating the inches multipled by the affected area to find the cubic mileage of rainfall
    
    String finalCubicMilesRainfall = String.format("%.8f", cubicMilesRainfall);
    //Changing the final answer into the corrected formatting for this assignment
    
    System.out.println(finalCubicMilesRainfall + "cubic miles");
     //Creating output message for the user denoting the total cubic mileage
}  //end of main method   
  	} //end of class
    
   
   