//Jason Nerreau
//CSE 2: Professor Brian Chen
//hw03 Objective: Practice in writing code that enables the user to input data and gives practice in performing artihmetic operations
//hw03 Task: Prompts user to input dimensions of a pyramid and outputs the calculated volume of this pyramid
//Date: 2/11/17

import java.util.Scanner;
//Import the Scanner function to be used within this HW Assignment

//Begin Public Class
public class Pyramid{
  //Main method to be used with every program within java
  public static void main(String[] args) {
  
    Scanner myScanner = new Scanner( System.in );
    //Telling the Scanner that you are creating an instance that will take input from STDIN
    
    System.out.print("The square side of the pyramid is (input length): ");
    //Telling user to type in the square side length of the pyramid 
    
    double squareSide = myScanner.nextDouble();
    //Accept user input
    
    System.out.print("The height of the pyramid is (input height): " );
    //Telling user to type in the height of the pyramid
    
    double height = myScanner.nextDouble();
    //Accept user input
    
    double base;
    //Create a variable base as a double type
    
    base = squareSide*squareSide;
    //Calculating the base variable as the length of the side squared
    
    double volumePyramid;
    //Creating the volume variable to be calculated
    
    volumePyramid = (base*height)/3;
    //Calculating the volume of the pyramid
    
    String finalVolumePyramid = String.format("%.4f", volumePyramid);
    //Changing the final answer into a more accurate format for this assignment
    
    System.out.println("The volume inside the pyramid is: " + finalVolumePyramid);
    //Creating output message for the user denoting the pyramid volume
}  //end of main method   
  	} //end of class
    
    
    
      
    
  