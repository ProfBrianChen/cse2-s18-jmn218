//CSE 2 hw07: Writing Methods 
//Professor Brian Chen
//Jason Nerreau: 3/26/18
//HW Objective: Practice in writing methods, overloading methods
//HW Task: Have the user input a shape and then the dimensions of the shape to calculate the area of said shape


//Import the scanner to be used for user input
import java.util.Scanner;

//Begin the public class with the name Area
public class Area{
  
//Declare variables to be used for the dimensions of each shape; must use static because they are used in multiple methods within this program and must be accesible by these methods
//Each variable is clearly named and the restarts are simply to reset the scanners if the user inputs the wrong type
static double rectangleHeight;
static double rectangleWidth;
static String restartRectangle;
static double triangleBase;
static double triangleHeight;
static String restartTriangle;
static double circleRadius;
static String restartCircle;
  
//Begin main method
  public static void main(String[] args){
  //Declaring the area variable to be used in calculating the area of whatever specified shape the user inputs
  double area;
  //Creating the scanner to be used under the name myScanner
  Scanner myScanner = new Scanner( System.in );
  //Telling user to enter a shape name 
  System.out.print("Enter either rectangle, triangle, or circle:  ");
  //Declaring userInput as a string and accepting the user input to this variable
  String userInput = myScanner.next();
  //Creating an integer variable named check to make it easier to compare the shapes and calculate the areas
  //Check calls the method named inputCheck using the userInput of the shape specified
  int check = inputCheck(userInput);
    
  //Using a while statement to check that a proper shape was input
  //From the input method, the number 4 for check corresponds to none of the correct shapes and thus asks the user to input a new shape and again calls the input method to check that it is corrcet
  while ( check == 4 ){
    System.out.print("Incorrect shape input. Please re-enter either a rectangle, triangle, or circle:  ");
    userInput = myScanner.next();
    check = inputCheck(userInput);
  } 
  //If the input method found that a rectangle was input, then this if statement is true and calls the rectangle area method to calculate the areas
  //This method requires the dimensions doubles which were input in the input method
  if ( check == 1)
  {
   area = rectangleArea(rectangleHeight,rectangleWidth);
   System.out.println("The area of the rectangle is:  " + area);
  }  
  //If the input method found that a triangle was input, then this if statement is true and calls the triangle area method to calculate the area
  //This method requires the dimensions doubles which were input in the input method
  else if ( check == 2)  
  {
    area = triangleArea(triangleBase,triangleHeight);
    System.out.println("The area of the triangle is:  " + area);
  }
  //If the input method found that a circle was input, then this if statement is true and calls the circle area method to calculate the areas
  //This method requires the dimensions doubles which were input in the input method
  else if ( check == 3)
  {
    area = circleArea(circleRadius);
    System.out.println("The area of the circle is:  " + area);
  }   
}
  
  
//Creating the method to check inputs of the user called inputCheck
public static int inputCheck(String i){
//Again initializing the scanner as this method requires users to input dimensions
Scanner myScanner = new Scanner( System.in );
//Stringing new variables for ease of conditional statements
String r = "rectangle";
String t = "triangle";
String c = "circle"; 

  //If the user input is equal to the string rectangle, then it tells users to input the dimensions of the rectangle
  //Because the required input is double, the while loop within the if statement checks to make sure that the scanner's most recent input has a double within it
  //If no double is found, the while loop repeats until the user correctly inputs a double
  //The 2 required inputs are rectangle height and rectangle width
  if ( i.equals(r) )
  {
    System.out.print("Enter height of rectangle:  ");
    while (!(myScanner.hasNextDouble())){
          restartRectangle = myScanner.next();
          System.out.print("Incorrect input form, please enter height of rectangle as a double:  ");
    }
    rectangleHeight = myScanner.nextDouble();
    
    System.out.print("Enter height of rectangle");
    while (!(myScanner.hasNextDouble())){
          restartRectangle = myScanner.next();
          System.out.print("Incorrect input form, please enter width of rectangle as a double:  ");
    }
    rectangleWidth = myScanner.nextDouble();
    return 1;  
  }  
   //If the user input is equal to the string triangle, then it tells users to input the dimensions of the triangle
  //Because the required input is double, the while loop within the if statement checks to make sure that the scanner's most recent input has a double within it
  //If no double is found, the while loop repeats until the user correctly inputs a double
  //The 2 required inputs are triangle base and triangle height 
  else if ( i.equals(t) )
  {
   System.out.print("Enter base of triangle:  ");
   while (!(myScanner.hasNextDouble())){
          restartTriangle = myScanner.next();
          System.out.print("Incorrect input form, please enter base of triangle as a double:  ");
   }
    triangleBase = myScanner.nextDouble();
     System.out.print("Enter height of triangle:  ");
   while (!(myScanner.hasNextDouble())){
          restartTriangle = myScanner.next();
          System.out.print("Incorrect input form, please enter height of triangle as a double:  ");
   }
    triangleHeight = myScanner.nextDouble();            
    return 2;
  }
  //If the user input is equal to the string circle, then it tells users to input the dimensions of the circle
  //Because the required input is double, the while loop within the if statement checks to make sure that the scanner's most recent input has a double within it
  //If no double is found, the while loop repeats until the user correctly inputs a double
  //The 2 required inputs are circle radius
  else if ( i.equals(c) )
  {
   System.out.print("Enter radius of circle:  ");
   while (!(myScanner.hasNextDouble())){
          restartCircle = myScanner.next();
          System.out.print("Incorrect input form, please enter radius of circle as a double:  ");
   }
    circleRadius = myScanner.nextDouble();  
    return 3;
  }
  else //Returns the number 4 which triggers the main method to ask the user to tell the user to input a new proper shape
  {
    return 4;
  }
}

 //Beginning the method to compute the rectangle area given both a width double and a height double 
 public static double rectangleArea(double width, double height){ 
   return width*height;
 }
 
 //Beginning the method to compute the triangle  area given both the base of the triangle and the height double
 public static double triangleArea(double base, double height2){
   return (.5)*base*height2;
 }
  
 //Beginnging the method to compute the circle area give the double radius
  public static double circleArea(double radius){
    return (3.14159)*radius*radius;
  }

}






   
   