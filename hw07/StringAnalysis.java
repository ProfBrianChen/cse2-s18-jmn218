//CSE 2 hw07: Writing Methods 
//Professor Brian Chen
//Jason Nerreau: 3/26/18
//HW Objective: Practice in writing methods
//HW Task: Have the user input a string and then test whether the string contains letters

//Import scanner to be used in this program
import java.util.Scanner;
//Create the public class called StringAnalysis
public class StringAnalysis{

//Begin main method
   public static void main(String[] args){
     //Declaring variables to be used throughout the program
     //Restart is simply a word to reset the scanner; userString is the actual input in string form that the user will type input
     //letterCheck is the boolean result of calling one of the methods; letterCheckInteger is the boolean result of calling the other method
      String restart;
      String userString;
      boolean letterCheck;
      boolean letterCheckInteger;
     
     //Declaring the scanner to be named myScanner
      Scanner myScanner = new Scanner( System.in );
      
     //Asking the user to enter a string
      System.out.print("Enter a string:  ");
     //Using a while statement to test whether the user has indeed entered a string even though every user input will be a string
     //If incorrect input, asks to enter again and then tests if the user is a string in the while loop again
      while (!(myScanner.hasNext())){
      restart = myScanner.next();
      System.out.print("Incorrect input, please re-enter a string");
      }
      userString = myScanner.next();
     
     //Asking the user whether they would like to analyze the string as a whole or, instead, enter an integer to specify a number of characters
     System.out.print("Would you like to analyze the entire string? (Yes/No) ");
     
     //Use a while loop to check whether the user input is a string while will always be true
     //userAnswer variable accepts the most recent user input and then the if statement compares this to yes, no, or something else
     //If yes is selected, then the testLetters method is called and then the statement will return result of this methods
     //If no is selected, then the testLettersIntegers method is called and then the statement will return result of this method
     //If an incorrect answer is selected, then it will ask the user to enter a correct response and run the loop again to check the answer
     while (myScanner.hasNext()){
     String userAnswer=myScanner.next();
      if ((userAnswer.equals("Yes"))){
        letterCheck = testLetters(userString);
        System.out.println("Whether or not the entire string contains letters is: " + letterCheck);
        break;
      }
       else if ((userAnswer.equals("No"))){
         letterCheckInteger = testLettersInteger(userString);
         System.out.println("Whether or not the string contains letters for the given integer is: " + letterCheckInteger);
         break;
       }
       else {
         System.out.print("Incorrect response. Please answer whether you would like to analyze the entire string with either a Yes or a No: ");
       }
     }
    }

  //Begin method testLettersInteger
  //This method takes the string of user input and then asks the user to enter an integer which is the amount of characters to be tested
  //By using a for loop, it begins at letter 0 and then increments by 1 up until i is equal to the integer specified
  //The counter will be incremented by 1 if a letter is found and then, at the end, the if statement tests whether or not the counter is greater than 0 therefore indicating letters within the string
  public static boolean testLettersInteger(String testing){
  Scanner myScanner = new Scanner( System.in );
  int counter = 0;
  System.out.print("Enter an integer to represent the amount of characters to check in the string: ");
  int userInt = myScanner.nextInt();
  int stringLength = testing.length();
  if (userInt > stringLength){
    userInt = stringLength;
  }
  else {
    userInt = userInt;
  }
  for (int i = 0; i < userInt; i++){
    if (Character.isLetter(testing.charAt(i)))
        counter++;
  } 
  if (counter > 0){
    return true;
  }
  else
  {
    return false;
  }
}

  //Begin method testLetters
  //This method takes the string of user input and tests each character in the string
  //By using a for loop, it begins at letter 0 and then increments by 1 up until i is equal to the length of the string
  //The counter will be incremented by 1 if a letter is found and then, at the end, the if statement tests whether or not the counter is greater than 0 therefore indicating letters within the string  
  public static boolean testLetters(String testing){
  int counter = 0;
  for (int i = 0; i < testing.length(); i++){
    if (Character.isLetter(testing.charAt(i)))
        counter++;
  }
  if (counter > 0){
    return true;
  }
  else {
    return false;
  }
  }  
}



      
      