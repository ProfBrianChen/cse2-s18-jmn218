//Cyclometer 
//02/02/18, Jason Nerreau
//Code Purpose:
//Prints the number of minutes for each trip
//Prints the number of counts for each trip
//Prints the distance of each trip in miles
//Prints the distance for the two trips combined


//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    int secsTrip1=480;  //Setting secsTrip1 variable as integer as 480
    int secsTrip2=3220;  //Setting secsTrip2 variable as integer as 3220
		int countsTrip1=1561;  //Setting countsTrip1 variable as integer as 1561
		int countsTrip2=9037; //Setting countsTrip2 variable as integer as 9037
	
    //Creating variables for useful constants 
    double wheelDiameter=27.0;  //Wheel Diameter constant variable
  	double PI=3.14159; //Pi constant variable
  	int feetPerMile=5280;  //feetPerMile constant variable
  	int inchesPerFoot=12;   //inchesPerFoot constant variable
  	double secondsPerMinute=60.0;  //secondsPerMinute constant variable
	  double distanceTrip1; //Setting distanceTrip1 as a double type
    double distanceTrip2; //Setting distanceTrip2 as a double type 
    double totalDistance;  //Setting totalDistance as a double type
   
     //Print out numbers that have been stored
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
      
    distanceTrip1=countsTrip1*wheelDiameter*PI;
      //Calculating distance traveled in inches 
    distanceTrip2=countsTrip2*wheelDiameter*PI;
      //Calculating distance traveled in miles
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      //Computing distance travelled in miles on the trip
    totalDistance=distanceTrip1+distanceTrip2;
      
     //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
      
    }  //end of main method   
} //end of class
