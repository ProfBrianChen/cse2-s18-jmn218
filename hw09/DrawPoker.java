//CSE 2 hw08: Draw Poker
//Professor Brian Chen
//Jason Nerreau: 4/17/2018
//HW Objective: Practice with manipulating arrays
//HW Task: Have Create 2 hands of 5-Card poker and then compare the scores based on the hands

//Begin the public class with the name Area
public class DrawPoker{
  
 //Begin main method
  public static void main(String[] args){
		//Create the deck array of 52 cards and assign values 0 to 51
    int[] deck = new int[52];
    for (int i = 0; i<52; i++){
      deck[i] = i;
    }
		
		//Create the two hand arrays of 5 cards each for each player
    int [] playerOne = new int[5];
    int [] playerTwo = new int[5];
    int counter = 0;
		
		//Call the method to shuffle the 52 card deck array
    DrawPoker.shuffle(deck);
    
    //Loop for assigning shuffled cards to each player using the first 10 cards of the now shuffled array
    for (int i = 0; i < 10; i++){
      if ( i%2 == 0){
        playerOne[counter] = deck[i];
      }
      else {
        playerTwo[counter] = deck[i];
        counter++;
      }
    }
      
    
    
    
    //Calling all the various scoring possibilities and returning boolean whether the methods find a pair, three of a kind, flush, or full house
		System.out.println("The first players hand numbers are ");
    boolean numPairsPlayer1 = pairs(playerOne);
		System.out.println("The second players hand numbers are ");
    boolean numPairsPlayer2 = pairs(playerTwo);
    boolean numThreesPlayer1 = threes(playerOne);
    boolean numThreesPlayer2 = threes(playerTwo);
		System.out.println("The first players hand suits are (0=Hearts, 1=Spades, 2=Diamonds, 3=Clubs) ");
    boolean flushP1 = flush(playerOne);
		System.out.println("The second players hand suits are (0=Hearts, 1=Spades, 2=Diamonds, 3=Clubs) ");
    boolean flushP2 = flush(playerTwo);
    boolean fullHouseP1 = fullHouse(numPairsPlayer1, numThreesPlayer1);
    boolean fullHouseP2 = fullHouse(numPairsPlayer2, numThreesPlayer2);

        
   //Printing the results of each method call for player 1 to make it easier to see the break down of how the final score is
      System.out.println("Player 1 Pairs: " + numPairsPlayer1);
      System.out.println("Player 1 Three-Of-A-Kind: " + numThreesPlayer1);
      System.out.println("Player 1 Flush: " + flushP1);
      System.out.println("Player 1 Full House: " + fullHouseP1);

		//Printing the results of each method call for player 2 to make it easier to see the break down of how the final score is
      System.out.println("Player 2 Pairs: " + numPairsPlayer2);
      System.out.println("Player 2 Three-Of-A-Kind: " + numThreesPlayer2);
      System.out.println("Player 2 Flush: " + flushP2);
      System.out.println("Player 2 Full House: " + fullHouseP2);
		
		//Creating two scoring variables that assign a numeric value based on the boolean results of each scoring possibility
		//Pairs = 1; Three-of-a kind = 2; Flush = 3; Full House = 4
		int scoreP1 = 0;
		int scoreP2 = 0;
		
		//If statements to assign numeric values based on boolean results
		if (numPairsPlayer1 == true){
			scoreP1 = 1;
		}
		if (numPairsPlayer2 == true){
			scoreP2 = 1;
		}
		if (numThreesPlayer1 == true){
			scoreP1 = 2;
		}
		if (numThreesPlayer2 == true){
			scoreP1 = 2;
		}
		if (flushP1 == true){
			scoreP1 = 3;
		}
		if (flushP2 == true){
			scoreP2 = 3;
		}
		if (fullHouseP1 == true){
			scoreP1 = 4;
		}
		if (fullHouseP2 == true){
			scoreP1 = 4;
		}
		
		//If the scores are equal; then call the method to determine max card in a hand and whoever has the maximum card wins by the high card scoring method
		if (scoreP1 == scoreP2){
			int maxCardP1 = max(playerOne);
			int maxCardP2 = max(playerTwo);
			if (maxCardP1>maxCardP2){
				System.out.println("Player 1 had a max card of " + maxCardP1 + " and Player 2 had a max card of " + maxCardP2 + " thus Player 1 wins");
			}
			else {
				System.out.println("Player 1 had a max card of " + maxCardP1 + " and Player 2 had a max card of " + maxCardP2 + " thus Player 2 wins");	
			}
		}
		
		//If the scores are not equal then use the numeric value based on the scoring results to determine the winner
		else {
			if (scoreP1 > scoreP2){
				System.out.println("Player 1 had a higher score than player 2");
			}
			else {
				System.out.println("Player 2 had a higher score than player 1");
			}
		} 
  }
      
 
	
	//Method to shuffle the cards
  public static int[] shuffle(int[] cards){
  int noOfCards = cards.length;
    for (int i = 0; i < noOfCards; i++){
      int s = (int)(Math.random()*(noOfCards-1));
      int temp = cards[s];
      cards[s] = cards[i];
      cards[i] = temp;
    }
    return cards;
  }
  
  //Method to Calculate the amount of pairs
  public static boolean pairs(int[] cards1){
	//Create a temporary new array so the array being called is not manipulated
	int[] cards = new int[cards1.length];
	for (int i=0; i<cards1.length; i++){
		cards[i] = cards1[i];
	}	
	int noOfCards = cards.length;
		
    //Make Each Card a realistic deck of cards number using the modulo function
    for (int i = 0; i < noOfCards; i++){
      cards[i] = cards[i]%13;  
    }
		//Printing out the hands to make it easy for user to see 
    for (int i = 0; i < cards.length; i++){
      System.out.print(cards[i] + "    ");
    }
    System.out.println("");
    boolean result = false;
    int[] temp = cards;
    //Using nested loops to compare each card to see if any match up and therefore results in a pair
    for (int j = 0; j < temp.length; j++){
      for (int k = j+1; k < temp.length; k++){
        if (temp[j] == cards[k]){
          result = true;
        }
      } 
    }
    return result;
  }
  
  //Method to Calculate the amount of three of a kinds
  public static boolean threes(int[] cards1){
		//Create a temporary new array so the array being called is not manipulated
     int[] cards = new int[cards1.length];
		for (int i=0; i<cards1.length; i++){
			cards[i] = cards1[i];
		}
		int noOfCards = cards.length;
    //Make Each Card a realistic deck of cards number using the modulo function
    for (int i = 0; i < noOfCards; i++){
      cards[i] = cards[i]%13;  
    }
    //Using nested loops to compare each card to see if any match up to two other cards in the deck and therefore results in a three of a kind
    boolean result = false;
    int[] temp = cards;
    int[] temp2 = cards;
    for (int j = 0; j < temp.length; j++){
      for (int k = j+1; k < temp.length; k++){
        for (int s = k+1; s < temp.length; s++){
        if ((temp[j] == temp2[k]) && (cards[s] == temp2[k])){
          result = true;
        }
        }
      } 
    }
    return result;
  }
  
	//Method to Calculate the amount of flushes
   public static boolean flush(int[] cards1){
		 //Create a temporary new array so the array being called is not manipulated
     int[] cards = new int[cards1.length];
   		for (int i=0; i<cards1.length; i++){
			cards[i] = cards1[i];
		}
		int noOfCards = cards.length;
		 //Divide the deck number by 13 to find what suit each card is
   	 	for (int i = 0; i < noOfCards; i++){
      	cards[i] = (cards[i])/13;  
    }
     for (int i = 0; i < cards.length; i++){
      System.out.print(cards[i] + "    ");
    }
     System.out.println("");
		 
    int[] temp = cards;
    int counter = 0;
    boolean result = false;
		 //Comparing each card to see whether each is from the same suit and therefore the entire hand would be a flush
    for (int j = 0; j < cards.length-1; j++){
      if (temp[j]==cards[j+1]){
        counter++;
      }  
        }
    if (counter == 4){
      result = true;
    }
    return result;
      }
  
  //Method to find full house
  public static boolean fullHouse(boolean x, boolean y){
		//Using the results of a pair and three of a kind to see if a full house is  found
    boolean result = false;
    if (x == true){
      if (y == true){
        result = true;
      }
    }
    return result;
  }

	//Method to find the high card in the deck
	public static int max(int[] cards1){
		int[] cards = new int[cards1.length];

	for (int i=0; i<cards1.length; i++){
		cards[i] = cards1[i];
	}
	int noOfCards = cards.length;
  //Make Each Card a realistic deck of cards number
   for (int i = 0; i < noOfCards; i++){
     cards[i] = cards[i]%13;  
    }
		int max = cards[0];
			for (int i = 1; i < cards.length; i++ ){
				if (cards[i] > max)
                 max = cards[i];
			}
		return max;
	}
    }
    
  
  
  


    