//CSE 2 hw10: Matrix Code
//Professor Brian Chen
//Jason Nerreau: 4/23/2018
//HW Objective: Practice with multi-dimensional arrays 
//HW Task: Have Create 2 hands of 5-Card poker and then compare the scores based on the hands


//Begin public class
public class hw10{

//Method that sets the dimensions of the city and assigns a population to each block
//Use a random number generator for dimensions and population
//Return the array to the main method
public static int[][] buildCity(int x){
  int eastWest = (int )(Math.random() * 5 + 10);   //Create the dimensions of the city
  int northSouth = (int )(Math.random() * 5 + 10);  //Create the dimensions of the city
  int[][] cityArray = new int[northSouth][eastWest];    //Create the initial array of dimensions just established
  for (int i=0; i < northSouth; i++){   //For loop to assign each block a population between 100 and 999
     for (int j=0; j<cityArray[i].length; j++){
        int population = (int)(Math.random()*900 + 100);
        cityArray[i][j] = population;    //Assign the block population
     }
  }
return cityArray; //Return the city array
}
  
  
//Method to display the array using the printf method
public static void display(int[][] x){ 
  for (int i = 0; i < x.length; i++){    //For loops to print out each array block's population
    for (int j = 0; j < x[i].length; j++){
          System.out.printf("%5d", x[i][j]);
        }
    System.out.printf("\n");
    System.out.printf("\n");
      } 
    }

//Method to create the robots and change the randomly invaded blocks to a negative value
//Print out the new array with the negative values representing the invaded blocks 
public static int[][] invade(int[][]x , int k){
  int maxNorth = x.length;      //Creating an int which is the amount of rows in the array
  int maxWest = x[1].length;     //Creating an int which is the amount of columns in the array
  for (int i = 0; i < k; i++){    //For loop based on the amount of robots invading
   int robotHorizontalLocation = (int)(Math.random()*maxWest);      //Randomly assigning a block to invade based on the dimensions of the array
   int robotVerticalLocation = (int)(Math.random()*maxNorth);
   x[robotVerticalLocation][robotHorizontalLocation] = -(x[robotVerticalLocation][robotHorizontalLocation]);    //Making the randomly invaded block negative
   }
    for (int i = 0; i < x.length; i++){     //For loop to print out the newly invaded city
    for (int j = 0; j < x[i].length; j++){
         System.out.printf("%5d", x[i][j]);
    }
    System.out.printf("\n");
    System.out.printf("\n");
    
    }
    return x;
  }

//Method to update the invaded city by moving the robots east
//Use for loop and if statements to see whether or not each array value is negative
//If it is, change that array value to a positive and change the next block to the east to be negative representing the robots invading that block
public static void update(int[][] x){
   for (int repeat = 0; repeat<5; repeat++){ //3 Nested for loops to check every block and repeat 5 times 
    for (int i = 0; i < x.length; i++){
      for (int j = 0; j<x[i].length; j++){
        if (x[i][j] < 0){               //Check if the city was negative and therefore invaded
          x[i][j] = -(x[i][j]);         //If it is, change it back to positive
          if ((j+1) < x[i].length){      //Checking to see if the next east city is within the array's bounds
            x[i][j+1] = -1*(x[i][j+1]);
            j++;
          }
        }
      }
    }
System.out.println("Update number " + (repeat+1));  
System.out.println("");
    for (int i = 0; i < x.length; i++){   //For loop to print out the newly updated array
    for (int j = 0; j < x[i].length; j++){
          System.out.printf("%5d", x[i][j]);
    }
    System.out.printf("\n");
        System.out.printf("\n");
    } 
  }
}
  
public static void main(String[] args){    //Begin main method
      int x = 1;                            
      int[][] cityArray = new int[15][15];        //Create the initial city array as a 15 by 15 
      cityArray = buildCity(x);                 //Call the method to build the array
      display(cityArray);                              //Call the method to display the array
      int columns = cityArray[1].length;              //Count how many columns    
      int rows = cityArray.length;                    //Count how many rows
      int totalBlocks = columns + rows;               //Take the total blocks
      int robots = (int)(Math.random()*totalBlocks);      //Create a number of robots within the bounds of the array
      System.out.println("An invasion of " + robots + " robots");       //Print out how many rows
      System.out.println("");                        
      int[][] invadedArray = new int[15][15];         //Create a new array that has been invaded
      invadedArray = invade(cityArray, robots);       //Call the method that invades 
      update(invadedArray);                           //Call the method to update the array
  }    
}
  
  

