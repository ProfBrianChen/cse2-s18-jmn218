//CSE2 Lab 3
//Prof. Brian Chen
//Date: 2/9/2018
//Jason Nerreau
//Lab Objective: Demonstrate how you can get input from the user and use that data to perform basic computations



import java.util.Scanner;
//Import the Scanner function to be used within Lab

// Begin Public Class
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );
          //Telling the Scanner that you are creating an instance that will take input from STDIN
          
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
          //Prompting user for the original cost of the check in proper format
          
          double checkCost = myScanner.nextDouble();
          //Accept user input
          
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
          //Print out the percentage tip that you wish to pay as a whole number
          double tipPercent = myScanner.nextDouble();
          //Accepting the input to the scanner
          tipPercent /= 100; 
          //We want to convert the percentage into a decimal value
            
          System.out.print("Enter the number of people who went out to dinner: ");
          //Prompting the user for the number of people that went to dinner and accept the input
          int numPeople = myScanner.nextInt();
          //Turn the numPeople variable into an integer to represent amount of people to split check
          
          double totalCost;
          //Turning totalCost variable into a double type
          double costPerPerson;
          //Turning costPerPerson variable into a double type
          
          int dollars;
          int dimes;
          int pennies;
          //whole dollar amount of cost
          //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
          totalCost = checkCost * (1 + tipPercent);
          //Calculation for totalCost variable
          costPerPerson = totalCost / numPeople;
          //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson;
          //get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
}  //end of main method   
  	} //end of class
