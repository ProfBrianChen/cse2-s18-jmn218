/////////////////////////////////////////
///CSE 2 Welcome Class
///Begin Main Method
public class WelcomeClass {
  public static void main(String[] args) {
   //Begin Writing Code for Message
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--M--N--2--1--8->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I Interned in Hong Kong this past summer");                   
  }
}