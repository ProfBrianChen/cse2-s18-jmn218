//CSE 2 hw04: Yahtzee Game
//Professor Brian Chen
//Jason Nerreau: 2/18/18
//HW Objective: Practice with slection statements, operators, and String Manipulation
//HW Task: Creat the class dice game of Yahtzee and computing the scoring result of the roll

import java.util.Scanner;
//Import the Scanner function to be used within this HW Assignment

//Begin public class named Yahtzee
public class Yahtzee{
  //Main method to be used with every program within javapublic
  public static void main(String[] args) {
    
  Scanner myScanner = new Scanner( System.in );
  //Telling the Scanner that you are creating an instance that will take input from STDIN
 
  //Ask user whether they would prefer to roll randomly or to input digits themselves
  System.out.print("Do you wish to input your own 5 digits to represent the dice spins? Answer 'Yes' or 'No': ");
  
  //Creating a variable for the response of the user
   
  String userAnswer = myScanner.nextLine();
System.out.print(userAnswer);
  
	//Creating variables to be used whether or not the user inputs the numbers or uses random number generator
	//Initializing counters that represent amount of times a dice side shows amongst 5 die 
	int ones = 0;
	int twos = 0;
	int threes = 0;
	int fours = 0;
	int fives = 0;
	int sixes = 0;
	//Variables that represent the scores for each possibility
	int yahtzee = 0;
	int threeOfAKind = 0;
	int fourOfAKind=0;
	int fullHouse = 0;
	int smallStraight = 0;
	int largeStraight = 0;
	int chance = 0;
	//Variable for the Lower Section score types
	int lowerSectionTotal;
	int grandTotal;
	
  //Testing what the user responded with
  if ( userAnswer.equals("Yes") ){
  //Telling user to type in a 5 digit value to represent 5 dice rolls  
		System.out.print("Enter a 5 digit number to represent 5 dice spins (Acceptable Values:1,2,3,4,5,6) ");
  //Accept user input as a string to be checked if in proper format
  	String userDiceSpin = myScanner.nextLine();
  //Create a variable to represent the amount of digits that the user typed in as an integer with name digitLength
  	int digitLength = userDiceSpin.length();
  
  //Check if the user input the digits in the proper format
    if ( String.valueOf(userDiceSpin).contains("0") ){
     	System.out.println("Incorrect digit input, exit and restart game");
     return;
     }
    else if( String.valueOf(userDiceSpin).contains("7") ){
     	System.out.println("Incorrect digit input, exit and restart game");
     return;
     }
    else if( String.valueOf(userDiceSpin).contains("8") ){
     System.out.println("Incorrect digit input, exit and restart game");
     return;
    }
    else if( String.valueOf(userDiceSpin).contains("9") ){
     System.out.println("Incorrect digit input, exit and restart game");
     return;
    }
    else if( digitLength != 5 ){
     System.out.println("Incorrect digit input, exit and restart game");
     return;
    }
    
   //Process to Find how many of each die
   //Convert string to integer, use modulo division to shorten digit and then use regular division and int to come up with die valueOf
   int userRoll = Integer.parseInt(userDiceSpin);
   int die1 = (int)(userRoll/10000);
   int die2 = (int)((userRoll%10000)/1000);
   int die3 = (int)((userRoll%1000)/100);
   int die4 = (int)((userRoll%100)/10);
   int die5 = (int)(userRoll%10);
   
   //Solving for the upperInitialTotal and upperTotalBonus using If-Else statement to determine if the bonus 35 points were reached
   int upperInitialTotal = die1 + die2 + die3 + die4 + die5;
	//Create the if statement to determine whether 63 points was reached and if true, then add the bonus of 35 to the upper total with bonus score
   int upperTotalBonus;
      if ( upperInitialTotal > 63) {
      upperTotalBonus = upperInitialTotal + 35;
      }
      else upperTotalBonus = upperInitialTotal;
		//Using a switch to see what the number is for die1, and if it is correct, the counter adds 1 to the corresponding die number variable
		switch ( die1 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		//Use the Counter to Figure Out what number die 2 is showing
				switch ( die2 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 3 is showing
		switch ( die3 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		//Use the Counter to Figure Out what number die 4 is showing
		switch ( die4 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 5 is showing
		switch ( die5 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		//Decision to test for Yahtzee, three of a kind, or four of a kind
		//By logic, if one is a yahtzee, then it cannot be a 3 or 4 of a kind and vice versa
		//Corresponding scores then take that variable's value
		if ( ones == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
		}
		//Test if there are 5 twos showing
			else if ( twos == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 threes showing
			else if ( threes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 fours showing
			else if ( fours == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 fives showing
			else if ( fives == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 sixes showing
			else if ( sixes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 4 ones showing
			else if ( ones == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 4;
			yahtzee = 0;
			}
		//Test if there are 4 twos showing
			else if ( twos == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 8;
			yahtzee = 0;
			}
		//Test if there are 4 threes showing
			else if ( threes == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 12;
			yahtzee = 0;
			}
		//Test if there are 4 fours showing
			else if ( fours == 4){
			threeOfAKind = 0;
			fourOfAKind = 16;
			yahtzee = 0;
			}
		//Test if there are 4 fives showing
			else if ( fives == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 20;
			yahtzee = 0;
			}
			//Test if there are 4 sixes showing
			else if ( sixes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Testing for a full house now using if nested if statements
		//If a side is showing 3 times, then to achieve a full house, another side must show in the other 2 die5
		//If this is correct then fullHouse value is 25 
			if( ones == 3 ){
				if (twos == 2){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}	
				else fullHouse = 0;
			threeOfAKind = 3;
			fourOfAKind = 0;
			yahtzee = 0;
			}
				//Test for 3 twos and a double of another side
			else if ( twos == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 6;
			fourOfAKind = 0;
			yahtzee = 0;
			}
				//Test for 3 threes and a double of another side
			else if ( threes == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 9;
			fourOfAKind = 0;
			yahtzee = 0;
			}
				//Test for 3 fours and a double of another side
			else if ( fours == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 12;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		//Test for 3 fives and a double of another side
			else if ( fives == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 15;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		//Test for 3 sixes and a double of another side
			else if ( sixes == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 18;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
		//Testing for a smallstraight by seeing if 4 in a row are showing in the first if statement
		//If it is true, then it applies the bonus for a small straight of 30 but also tests to see if the next die completes the largestraight
		//If this is true, then largestraight bonus of 40 is applied while the small straight reverts back to a value of 30
		//Check for a straight beginning with 1-2-3-4; the dice can potentially be shown on 2 die hence the "or" operators
		if ( ((ones == 1) || (ones == 2)) && ((twos == 1) || (twos == 2)) && ((threes == 1) || (threes == 2)) && ((fours == 1) || (fours == 2)) ){
			smallStraight = 30;
			if ( (fives == 1) || (fives == 2) ){
				largeStraight = 40;
				smallStraight = 0;
			}
			//If the large straight statment false, then large straight value takes on zero
			else largeStraight = 0;
		}
		//Check for another straight of 2-3-4-5
		else if ( ((twos == 1) || (twos == 2)) && ((threes == 1) || (threes == 2)) && ((fours == 1) || (fours == 2)) && ((fives == 1) || (fives == 2)) ){
			smallStraight = 30;
			if ( sixes == 1 ){
				largeStraight = 40;
				smallStraight = 0;
			}
			else largeStraight = 0;
		}
		//Check for another straight of 3-4-5-6
		else if ( ((threes == 1) || (threes == 2)) && ((fours == 1) || (fours == 2)) && ((fives == 1) || (fives == 2)) && ((sixes == 1) || (sixes == 2)) ){
			smallStraight = 30;
		}
		else smallStraight = 0;
		
		
		
		//Printing out the upper Initial Total and the upper Total with the bonus
    System.out.println("The upper section initial total is " + upperInitialTotal);
    System.out.println("The upper section total with the bonus is " + upperTotalBonus);
		
		//Use the sum of the upper section, pre bonus as the chance value as it represents the sum of the dice sides showing
		chance = upperInitialTotal;
		
		//Sum up all of the rows within the lower section to come up with the total of lower section total
		lowerSectionTotal = threeOfAKind + fourOfAKind + yahtzee + fullHouse + smallStraight + largeStraight + chance;
		//Print the lower section total
		System.out.println("The lower section total is: " + lowerSectionTotal);
		//Calculate the grand total as the upper section total with the bonus plus the lower section total
		grandTotal = upperTotalBonus + lowerSectionTotal;
		//Print out the grand total
		System.out.println("The grand total is: " + grandTotal);
		}
	
		
		
		
		
	//If user does not want to input 
  else if ( userAnswer.equals("No") ){
	//Creating 5 random dicespins into integers ranging between 1 and 6
  int randDie1 = (int)(Math.random()*6+1);
  int randDie2 = (int)(Math.random()*6+1);
  int randDie3 = (int)(Math.random()*6+1);
  int randDie4 = (int)(Math.random()*6+1);
  int randDie5 = (int)(Math.random()*6+1);

  System.out.println("The Randomly generated 5 die showed the numbers: " + randDie1 + randDie2 + randDie3 + randDie4 + randDie5);
	//Solving for the upperInitialTotal and upperTotalBonus using If-Else statement to determine if the bonus 35 points were reached
   int upperInitialTotal = randDie1 + randDie2 + randDie3 + randDie4 + randDie5;
   int upperTotalBonus;
      if ( upperInitialTotal > 63 ) {
      upperTotalBonus = upperInitialTotal + 35;
      }
      else upperTotalBonus = upperInitialTotal;
          
  //Using a switch to see what the number is for die1, and if it is correct, the counter adds 1 to the corresponding die number variable
		switch ( randDie1 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 2 is showing
				switch ( randDie2 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 3 is showing
		switch ( randDie3 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 4 is showing
		switch ( randDie4 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Use the Counter to Figure Out what number die 5 is showing
		switch ( randDie5 ){
			case 1:
				ones++;
				break;
			case 2:
				twos++;
				break;
			case 3:
				threes++;
				break;
			case 4: 
				fours++;
				break;
			case 5: 
				fives++;
				break;
			case 6:
				sixes++;
				break;
		}
		
		//Decision to test for Yahtzee, three of a kind, or four of a kind
		//By logic, if one is a yahtzee, then it cannot be a 3 or 4 of a kind and vice versa
		//Corresponding scores then take that variable's value
		if  ( ones == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
		}
		//Test if there are 5 twos showing
			else if ( twos == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 threes showing
			else if ( threes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 fours showing
			else if ( fours == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 fives showing
			else if ( fives == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 5 sixes showing
			else if ( sixes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		//Test if there are 4 ones showing
			else if ( ones == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 4;
			yahtzee = 0;
			}
		//Test if there are 4 twos showing
			else if ( twos == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 8;
			yahtzee = 0;
			}
		//Test if there are 4 threes showing
			else if ( threes == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 12;
			yahtzee = 0;
			}
		//Test if there are 4 fours showing
			else if ( fours == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 16;
			yahtzee = 0;
			}
		//Test if there are 4 fives showing
			else if ( fives == 4 ){
			threeOfAKind = 0;
			fourOfAKind = 20;
			yahtzee = 0;
			}
			//Test if there are 4 sixes showing
			else if ( sixes == 5 ){
			threeOfAKind = 0;
			fourOfAKind = 0;
			yahtzee = 50;
			}
		
		//Testing for a Full House now using if nested if statements
		//This assumes that if a 3 of a kind and a full house is acheived, then you are awarded points for both the 3 of a kind and the full house
		//If a side is showing 3 times, then to achieve a full house, another side must show in the other 2 die5
		//If this is correct then fullHouse value is 25 
			else if( ones == 3 ){
				if (twos == 2){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}	
				else fullHouse = 0;
			threeOfAKind = 3;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
				//Full House Test: 3 twos and a double of another side
			else if ( twos == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 6;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
				//Full House Test: 3 threes and a double of another side
			else if ( threes == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 9;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
				//Full House Test: 3 fours and a double of another side
			else if ( fours == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 12;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
		//Full House Test: 3 fives and a double of another side
			else if ( fives == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( sixes == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 15;
			fourOfAKind = 0;
			yahtzee = 0;
			}
		
		//Full House Test: 3 sixes and a double of another side
			else if ( sixes == 3 ){
				if ( ones == 2 ){
					fullHouse = 25;
				}
				else if( twos == 2 ){
					fullHouse = 25;
				}
				else if( threes == 2 ){
					fullHouse = 25;
				}
				else if( fours == 2 ){
					fullHouse = 25;
				}
				else if( fives == 2 ){
					fullHouse = 25;
				}
				else fullHouse = 0;
			threeOfAKind = 18;
			fourOfAKind = 0;
			yahtzee = 0;
			}

		//Testing for a smallstraight by seeing if 4 in a row are showing in the first if statement
		//If it is true, then it applies the bonus for a small straight of 30 but also tests to see if the next die completes the largestraight
		//If this is true, then largestraight bonus of 40 is applied while the small straight reverts back to a value of 30
		//Check for a straight beginning with 1-2-3-4; the dice can potentially be shown on 2 die hence the "or" operators
		if ( ((ones == 1) || (ones == 2)) && ((twos == 1)||(twos == 2)) && ((threes == 1)||(threes == 2)) && ((fours == 1)||(fours == 2)) ){
			smallStraight = 30;
			if ( (fives == 1)||(fives == 2) ){
				largeStraight = 40;
				smallStraight = 0;
			}
			//If the large straight statment false, then large straight value takes on zero
			else largeStraight = 0;
		}
		//Check for another straight of 2-3-4-5
		else if ( ((twos == 1) || (twos == 2)) && ((threes == 1) || (threes == 2)) && ((fours == 1) || (fours == 2)) && ((fives == 1) || (fives == 2)) ){
			smallStraight = 30;
			if ( sixes == 1 ){
				largeStraight = 40;
				smallStraight = 0;
			}
			else largeStraight = 0;
		}
		//Check for another straight of 3-4-5-6
		else if ( ((threes == 1) || (threes == 2)) && ((fours == 1) || (fours == 2)) && ((fives == 1) || (fives == 2)) && ((sixes == 1) || (sixes == 2)) ){
			smallStraight = 30;
		}
		else smallStraight = 0;
		
		System.out.println("the small straight score is " + smallStraight);
		
		//Print out the numbers that were given to each randomly chosen die
		System.out.println("The first die showed " + randDie1);
		System.out.println("The second die showed " + randDie2);
		System.out.println("The third die showed " + randDie3);
		System.out.println("The fourth die showed " + randDie4);
		System.out.println("The fifth die showed " + randDie5);
		
		//Printing out the upper Initial Total and the upper Total with the bonus
    System.out.println("The upper section initial total is " + upperInitialTotal);
    System.out.println("The upper section total with the bonus is " + upperTotalBonus);
		
		//Use the sum of the upper section, pre bonus as the chance value as it represents the sum of the dice sides showing
		chance = upperInitialTotal;
		
		//Sum up all of the rows within the lower section to come up with the total of lower section total
		lowerSectionTotal = threeOfAKind + fourOfAKind + yahtzee + fullHouse + smallStraight + largeStraight + chance;
		//Print out the lower section total
		System.out.println("The lower section total is: " + lowerSectionTotal);
		//Calculate the grand total as the upper total with bonus plus the lower section total
		grandTotal = upperTotalBonus + lowerSectionTotal;
		//Print out the grand total
		System.out.println("The grand total is: " + grandTotal);
	}  
	//Print out "Incorrect answer format" and end game if either "Yes" or "No' were not typed in properly
  else {
  System.out.println("Incorrect answer format");
   return;
	}
       }  //end of main method   
} //end of class  
    
    
    
    