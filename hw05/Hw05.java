//CSE 2 hw05: Reading In Loops
//Professor Brian Chen
//Jason Nerreau: 3/05/18
//HW Objective: Practice in loops. 
//HW Task: Your program is to write loops that asks the user to enter information relating to a course they are currently taking
//HW Task Specifics: Asl for course number, department name, the number of times it meets in a week, the time the class starts, the instructor name, and the number of students

import java.util.Scanner;
//Import the Scanner function to be used within this HW Assignment

//Begin public class named Hw05
public class Hw05{
  //Main method to be used with every program within javapublic
  public static void main(String[] args) {
    
  Scanner myScanner = new Scanner( System.in );
  //Telling the Scanner that you are creating an instance that will take input from STDIN
    String department;
    String instructor;
    int courseNumber = 0;
    //Initialize the course number as an integer with beginning value 0
    System.out.print("Enter Course Number: ");
    //Tell the user to input the course number 
    boolean courseNumberCheck = myScanner.hasNextInt();
    //Use this method to check whether the user input is a boolean value and return the variable courseNumberCheck as either true or false
    String restart1;
    //Declared a string variable as restart1 to be used to be used to remove the most recently entered user input if it is incorrect form
   
    while (!courseNumberCheck){
    //checking to see if user input was not an integer, if it is not then with the !, the while loop will read as true and run
    restart1 = myScanner.next();
    //using the junk word restart1 to remove the most recent user input from the scanner
    System.out.print("Incorrect course number format. Enter new course number in correct integer form: " );
    //Asking the user to re-input a course number in the proper form
    courseNumberCheck = myScanner.hasNextInt();
    //Re-check and return a boolean value if the new user input is indeed an integer
    }
    courseNumber = myScanner.nextInt();
    //Once the user input is an actual integer, the while loop will read a value of false and therefore not run
    //courseNumber is assigned the most recent user input which is indeed an integer in correct format
    
    
    
    System.out.print("Enter the department name: ");
    //Tell the user to input the department name 
    boolean departmentNameCheck = myScanner.hasNext();
    //Use this method to check whether the user input is a boolean value and return the departmentNameCheck as either true or false
    
    
    while (!departmentNameCheck){
    //While loop that runs if the the user input is not in the correct form of a string, however, user input will always be in the form of a string regardless of what is typed in
      department= myScanner.next();
      //using the junk word restart1 to remove the most recent user input from the scanner
      System.out.print("Incorrect department name. Enter new department name: " );
      //Asking the user to re-input a course number in the proper form
      departmentNameCheck = myScanner.hasNext();
      //Re-check and return a boolean value if the new user input is indeed an integer
    }
    department = myScanner.next(); 
    //if the user entered correct input, then this gets the department name 
     
    
    
    int weeklyMeetings = 0;
    //Initialize the number of weekly meetings as an integer with beginning value 0
    System.out.print("Enter the amount of times your class meets: ");
    //Tell the user to input the weekly meetings amount 
    boolean weeklyMeetingsCheck = myScanner.hasNextInt();
    //Use this method to check whether the user input is a boolean value and return the variable weekly meetings as either true or false
    String restart2;
    //Declared a string variable as restart2 to be used to be used to remove the most recently entered user input if it is incorrect form
    
    while (!weeklyMeetingsCheck){
    //checking to see if user input was not an integer, if it is not then with the !, the while loop will read as true and run
    restart2 = myScanner.next();
    //using the junk word restart2 to remove the most recent user input from the scanner
    System.out.print("Incorrect weekly meetings amount input. Enter new weekly meetings amount in correct integer form: " ); 
    //Asking the user to re-input the amount of weekly meetings in the proper form
    weeklyMeetingsCheck = myScanner.hasNextInt();
    //Re-check and return a boolean value if the new user input is indeed an integer  
    }
    weeklyMeetings = myScanner.nextInt();
    //Once the user input is an actual integer, the while loop will read a value of false and therefore not run
    //courseNumber is assigned the most recent user input which is indeed an integer in correct format
    
    
    
    
    int meetingHour = 0;
    //Declare and initialize the class meeting hour as 0
    System.out.print("Enter the hour of day the class meets: ");
    //Tell user to input the hour of the day that the class meets
    boolean hourCheck = myScanner.hasNextInt();
    //Use this method to check whether the user input is a boolean value and return the variable hourCheck as either true or false
    String restart3;
    //Declared a string variable as restart3 to be used to be used to remove the most recently entered user input if it is incorrect form
   
    while (!hourCheck){
    //checking to see if user input was not an integer, if it is not then with the !, the while loop will read as true and run
    restart3 = myScanner.next();
    //using the junk word restart 3 to remove the most recent user input from the scanner
    System.out.print("Incorrect hour of meeting time input. Enter new hour that the class meets in proper integer format: " ); 
    //Asking the user to re-input the hour of class time in the proper form  
    hourCheck = myScanner.hasNextInt();
    //Re-check and return a boolean value if the new user input is indeed an integer 
      } 
    meetingHour = myScanner.nextInt();
    //Create variable for meetingHour based on what was most recently input into scanner
    
    //Check that hour is in the proper format using a while looop
    while(meetingHour < 0 || meetingHour > 23){
      //While the integer is out of the range of unacceptable values this while loop will run
      System.out.print("The hour you entered is not a real hour. Enter an integer between 0 and 23: ");
      //Print statement to tell user to re enter an acceptable value
      if(!myScanner.hasNextInt()){
        //Check to make sure that the user inputs an integer
            System.out.print("Incorrect hour of meeting time input. Enter new hour that the class meets in proper integer format: " );
        //If user does not input an integer, tell user to enter an integer
            String skip = myScanner.next(); 
        //Remove the most recently entered value into the scanner
      }
      meetingHour = myScanner.nextInt(); 
      //Assign a proper hour variable for meetingHour based on what was most recently input into scanner
    }

  
    
  System.out.print("Enter the minute of the day the class meets: " );
  //Tell user to input the minute of the day that the class meets
  boolean minuteCheck = myScanner.hasNextInt();
  //Use this method to check whether the user input is a boolean value and return the variable minuteCheck as either true or fals
  int meetingMinute = 0;
  //Declare and initialize the class meeting minute as 0   
  String restart4;
  //Declared a string variable as restart3 to be used to be used to remove the most recently entered user input if it is incorrect form

  while (!minuteCheck){
     //checking to see if user input was not an integer, if it is not then with the !, the while loop will read as true and run
    restart4 = myScanner.next();
    //using the junk word restart 4 to remove the most recent user input from the scanner
    System.out.print("Incorrect minute of meeting time input. Enter new minute that the class meets in proper integer format: " );
   //Asking the user to re-input the minute of class time in the proper form 
    minuteCheck = myScanner.hasNextInt();
    //Re-check and return a boolean value if the new user input is indeed an integer 
  }
    meetingMinute = myScanner.nextInt();
    
//Check that minute is in the proper format using a while looop
    while(meetingMinute < 0 || meetingMinute > 59){
      //While the integer is out of the range of unacceptable values this while loop will run
      System.out.print("The minute you entered is not a real minute. Enter an integer between 0 and 59: ");
      //Print statement to tell user to re enter an acceptable value
      if(!myScanner.hasNextInt()){
        //Check to make sure that the user inputs an integer
            System.out.print("Incorrect hour of meeting time input. Enter new minute that the class meets in proper integer format: " );
        //If user does not input an integer, tell user to enter an integer
            String skip = myScanner.next(); 
        //Remove the most recently entered value into the scanner
      }
      meetingMinute = myScanner.nextInt(); 
      //Assign a proper minute variable for meetingHour based on what was most recently input into scanner
    }
    
    
    System.out.print("Enter the instructor's name: ");
    //Tell the user to input the instructor's name 
    boolean instructorNameCheck = myScanner.hasNext();
    //Use this method to check whether the user input is a boolean value and return the instructor name as either true or false
    
    
    while (!instructorNameCheck){
    //While loop that runs if the the user input is not in the correct form of a string, however, user input will always be in the form of a string regardless of what is typed in
      instructor = myScanner.next();
      //using the junk word restart1 to remove the most recent user input from the scanner
      System.out.print("Incorrect instructor name format. Enter new instructor name in form of string: " );
      //Asking the user to re-input a course number in the proper form
      instructorNameCheck = myScanner.hasNext();
      //Re-check and return a boolean value if the new user input is indeed an integer
    }
    instructor = myScanner.next();
    //Variable named instructor takes the last entered string value 
    
    
    int numberStudents = 0;
    //Declare and initialize the number of students as 0
    System.out.print("Enter the Number of Students in the class: ");
    //Tell user to input the amount of students in the class
    boolean studentCheck = myScanner.hasNextInt();
    //Use this method to check whether the user input is a boolean value and return the variable studentCheck as either true or fals
    String restart5;
    //Declared a string variable as restart5 to be used to be used to remove the most recently entered user input if it is incorrect form
    
    while (!studentCheck){
    //checking to see if user input was not an integer, if it is not then with the !, the while loop will read as true and run
    restart5 = myScanner.next();
    //using the junk word restart5 to remove the most recent user input from the scanner 
    System.out.print("Incorrect amount of students number format. Enter new amount of students in the class in correct integer form: " ); 
    //Asking the user to re-input the minute of class time in the proper form 
    studentCheck = myScanner.hasNextInt();
    //Re-check and return a boolean value if the new user input is indeed an integer
    }
    numberStudents = myScanner.nextInt();
    //Once the user input is an actual integer, the while loop will read a value of false and therefore not run
    //courseNumber is assigned the most recent user input which is indeed an integer in correct format
    
    
    
    
    
    System.out.println("The course number of this class is: " + courseNumber);
    System.out.println("This class is within the " + department + " department");
    System.out.println("This class meets " + weeklyMeetings + " times a week");
    System.out.println("The meeting time for this class is " + meetingHour + ":" + meetingMinute);
    System.out.println("This class is taught by " + instructor);
    System.out.println("There are " + numberStudents + " enrolled in this class");
    

    
    
    
  }
}
    
    
    
  
    