//CSE 2 hw08: Fun With Searching
//Professor Brian Chen
//Jason Nerreau: 4/08/2018
//HW Objective: Practice with arrays and in searching single dimensional arrays
//HW Task: Have the user enter ints, place these in an array and then search the array for the grades.HW


//Import the scanner to be used for user input
import java.util.Scanner;
import java.util.Random;

//Begin the public class with the name Area
public class CSE2Linear{
  
 //Begin main method
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    final int NUM_GRADES = 15;
    int[] studentGrades = new int[NUM_GRADES];
    int i = 0;
    
    for(i = 0; i < NUM_GRADES; i++){
      System.out.println(" Enter 1 of 15 grades in ascending ints for the final grades in CSE2:  ");
       
      //Testing to make sure that the integers are entered and not any other type of variables
      while (!myScanner.hasNextInt()){
        String restart = myScanner.next();
        System.out.print(" Integer not entered, please re-Enter an integer for a grade (0-100):  ");
      }
        studentGrades[i] = myScanner.nextInt();
      
      
      //Testing to make sure that the integers are in the correct range of grade values
      while ( (studentGrades[i] > 100) || (studentGrades[i] < 0) ){
        System.out.print(" Integer out of range, please re-Enter an integer for a grade (0-100):  ");
        while (!myScanner.hasNextInt()){
        String restart = myScanner.next();
        System.out.print(" Integer not entered, please re-Enter an integer for a grade (0-100):  ");
      }
        studentGrades[i] = myScanner.nextInt();
      }
      
      //Testing to make sure that the integers entered are in ascending order
      if (i>0){
      while ( studentGrades[i] < studentGrades[i-1] ){
        System.out.print(" Integer must be greater than the last entered integer. Please enter a number greater than " + studentGrades[i-1] + "  ");
       while (!myScanner.hasNextInt()){
        String restart = myScanner.next();
        System.out.print(" Integer not entered, please re-Enter an integer for a grade (0-100):  ");
      }
        studentGrades[i] = myScanner.nextInt();
      }
     
} 
  }
    
    
    
    
   //Print out the array of grades 
    System.out.print ("The array values are:  ");
    for(i = 0; i < NUM_GRADES; i++){
      System.out.print("  " + studentGrades[i] + "  ");
    }
    System.out.println("");
   
    //Ask user for a specific grade to look for
      System.out.println("Enter a grade to search for:  ");
      int gradeSearch1 = myScanner.nextInt();

    int searchPos = binarySearch(studentGrades, gradeSearch1);
    if (searchPos < 0) {
        System.out.println("Element not found   ");
    }
    else {
        System.out.println("Element found in the position  " + searchPos + " ");
    }

    int length = studentGrades.length;
     random(studentGrades, length);
    System.out.println("Enter another grade to search for:  ");
    int gradeSearch2 = myScanner.nextInt();
    String linearSearch = "";
    System.out.println(linearSearch(studentGrades, length, gradeSearch2));
  }
  
 

     
  
  //Method that randomizes the array
  public static void random(int[] arr, int x){
    Random r = new Random();
        r.nextInt();
    System.out.println("Scrambled:  ");
        for (int i = x-1; i > 0; i--) {
          int j = r.nextInt(i);
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
          System.out.print("  " + arr[i] + "  ");
        }
    System.out.println("");
    }
  
  
  //Method for the linear search
  public static String linearSearch(int [] arr, int n, int x){
    String answer = "";
    for (int i = 0; i < n; i++){
      if (arr[i]==x){
        answer = "Element found";
        break;
      }
      else {
        answer = "Element not found";
        }
    }
    return answer;
  }

  
  
  
  //Method for the binary search
  public static int binarySearch(int[] arr, int key) {
  int lo = 0;
        int hi = arr.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (key < arr[mid]){
              hi = mid - 1;
            }
            else if (key > arr[mid]){
              lo = mid + 1;
            } 
            else return mid;
        }
        return -1;
    }
  
}

 

    
  
  
  
  
  

