//CSE 2 hw08: Fun With Searching
//Professor Brian Chen
//Jason Nerreau: 4/08/2018
//HW Objective: Practice with arrays and in searching single dimensional arrays
//HW Task: Randomly generate an array, have the user input an index value to remove from the array, and then have the user input a value to remove from the new array


//Import the scanner
import java.util.Scanner;

//Create the class
public class RemoveElements{
	//Begin the main method
  public static void main(String [] arg){
	
	//Declare and initialize the scanner
	Scanner scan=new Scanner(System.in);
	//Declare the various arrays to be used
	int num[]=new int[10];
	int newArray1[];
	int newArray2[];
	int index,target;
	String answer="";
	//Do loop to perform the various method calls
	do{
		//First Method Call
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(1);
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
		
		//Second Method Call
		System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
		
		//Third method call
		System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	
		//Ask user whether they would like to start the loop again or not
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
	//Method to convert arrays into a string
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
	
	//First Method to create an array of 10 random numbers between 0 and 10
	//Use a for loop 10 times with a random number generator to assign values for each position in the array
	public static int[] randomInput(int x){
		int[] numbers = new int[10];       
    for(int i = 0; i < numbers.length; i++) {
      numbers[i] = (int)(Math.random()*10);
    }//end for loop
    return numbers;
}
	
	
	//Second Method to delete the specified index from the randomly created array
	//Create a counter array with the length of the original array (10)
	//Create a counter that will be incemented whenever the for loop counter does not match up to the specified index value
	//When these values do not match up, the counter array will take the value of the original array in that position and will skip over this when the i value does match to the specified index value
	public static int[] delete (int[] arr, int x){
		int originalLength = arr.length;
		int[] counterArray = new int[arr.length];
		int counter = 0;
		for(int i=0;i<originalLength; ++i){
    		if( i != x ){
        		counterArray[counter] = arr[i];
        		counter++;
    }	
}
		//Need to reduce the array by one value
		//Use a for loop for original length minus 1 to fill new array with the couter array
		int newLength = counterArray.length - 1;
		int[] newArray = new int[newLength];
		for (int i = 0; i<newLength; i++){
	  newArray[i]=counterArray[i];
		}
return newArray;
	}
	
	
	//Third method to delete the specified index from the new array
	//Create a counter array with the length of the new array
	//Create a counter that will be incemented whenever the new array value does not match up to the sspecified value
	//When these values do not match up, the counter array will take the value of the original array in that position and will skip over this when the i value does match to the specified index value
	//If the values do match, a remove amount will count up to be removed later on to return the final array
	public static int[] remove(int[] arr, int x){
		int originalLength = arr.length;
		int[] counterArray = new int[arr.length];
		int counter = 0;
		int amountRemoved = 0;
		for(int i=0;i<originalLength; ++i){
    		if( arr[i] != x ){
        		counterArray[counter] = arr[i];
        		counter++;
    }
			else {
				amountRemoved += 1;
			}
}
		int newLength = counterArray.length - amountRemoved;
		int[] newArray = new int[newLength];
		for (int i = 0; i<newLength; i++){
	  newArray[i]=counterArray[i];
		}
return newArray;
	}
	}

	
  
  
  
  
  

