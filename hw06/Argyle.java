import java.util.Scanner;

public class Argyle{
  public static void main(String[] args){
  Scanner myScanner = new Scanner(System.in);
  
  System.out.print("please enter a positive integer for the width of Viewing window in characters.");
  String temp1 = myScanner.next();
	char char1 = temp1.charAt(0);
  int width = (int)char1;
  
    
  System.out.println(width);
    
    
  System.out.print("please enter a positive integer for the height of Viewing window in characters.");
  String temp2 = myScanner.next();
	char char2 = temp2.charAt(0);
  int height = (int)char2;
    
  System.out.println(height);
    
    
   System.out.print("please enter a positive integer for the width of the argyle diamonds in characters.");
  String temp3 = myScanner.next();
	char char3 = temp3.charAt(0);
  int widthDiamonds = (int)char3;  
    
  System.out.println(widthDiamonds);
    
    
  System.out.print("please enter a positive odd integer for the width of the argyle center stripe.");
  String temp4 = myScanner.next();
	char char4 = temp4.charAt(0);
  int widthStripe = (int)char4; 
  String restart;
    
  while (!(widthStripe % 2 == 1)){
    System.out.print("Incorrect entry for widthStrip, please enter a positive odd integer for the width of the argyle center stripe." ); 
    temp4 = myScanner.next();
    char4 = temp4.charAt(0);
    widthStripe = (int)char4;
  }
   
  while (widthStripe > (widthDiamonds/2)){
    System.out.print("Incorrect entry for widthStrip, please enter a positive odd integer for the width of the argyle center stripe." ); 
    temp4 = myScanner.next();
    char4 = temp4.charAt(0);
    widthStripe = (int)char4;
  }
  
  System.out.println(widthStripe);
  
 System.out.print("please enter a first character for the pattern fill.");
  String temp5 = myScanner.next();
	char first = temp5.charAt(0);
 
   
  System.out.println(first);
  
  System.out.print("please enter a second character for the pattern fill.");
  String temp6 = myScanner.next();
	char second = temp6.charAt(0);
  while (first == second ){
   System.out.print("The first and second fill characters must be different, please enter another second character for the pattern fill."); 
      temp6 = myScanner.next();
      second = temp6.charAt(0);
 }  
    System.out.println(second);
  
   System.out.print("please enter a third character for the pattern fill.");
  String temp7 = myScanner.next();
	char third = temp7.charAt(0);
  
  System.out.println(third);
  
    
}
}


