/////////////////////////////////////////
///CSE 2 Arithmetic HW02
///Jason Nerreau Jmn218
///Date: 2-4-18 
///HW Objective: Practice manipulating data in stored variables, running simple calculations and in printing numerical output generated
///Begin Main Method
public class Arithmetic {
  public static void main(String[] args) {
    
   
    //Input Variable Assumptions
       //Number of pairs of pants
           int numPants = 3;
      //Cost per pair of pants
           double pantsPrice = 34.98;
      //Number of sweatshirts
          int numShirts = 2;
       //Cost per shirt
           double shirtPrice = 24.99;
      //Number of belts
          int numBelts = 1;
      //cost per box of envelopes
          double beltCost = 33.99;
      //the tax rate
          double paSalesTax = 0.06;
    
    //Declare Total Cost Variables of each Item
          double totalCostOfPants;
          double totalCostOfShirts;
          double totalCostOfBelts;
    //Declare Sales Tax Variables of each Item
          double salesTaxPants;
          double salesTaxShirts;
          double salesTaxBelts;
            
     //Declare PreTax Total Variable and Sales Tax Total Variable
         double totalCostPreTax;
         double totalSalesTax;
            
      //Declare Total Money Paid on Transaction Variable
         double CombinedCost;
   
            
            //Calculating Total Cost for each item
            totalCostOfPants=numPants*pantsPrice;
            totalCostOfShirts=numShirts*shirtPrice;
            totalCostOfBelts=numBelts*beltCost;
            
            //Display Total Cost of Each Item
            System.out.println("Total spent on Pants is "+ totalCostOfPants);
       	    System.out.println("Total spent on Shirts is "+ totalCostOfShirts);
            System.out.println("Total spent on Belts is "+ totalCostOfBelts);
    
            //Calculating Sales Tax for each item
            salesTaxPants=totalCostOfPants*paSalesTax;
            salesTaxShirts=totalCostOfShirts*paSalesTax;
            salesTaxBelts=totalCostOfBelts*paSalesTax;
    
           //Convert Sales Tax into Proper 2-Decimal Form
            String newSalesTaxPants = String.format("%.2f", salesTaxPants);
            String newSalesTaxShirts = String.format("%.2f", salesTaxShirts);
            String newSalesTaxBelts = String.format("%.2f", salesTaxBelts);
              
            //Display Sales Tax of Each Item
             System.out.println("Total Sales Tax on Pants is "+ newSalesTaxPants);
             System.out.println("Total Sales Tax on Shirts is "+ newSalesTaxShirts);
             System.out.println("Total Sales Tax on Belts is "+ newSalesTaxBelts);
    
    //Calculate Total PreTax Cost
        totalCostPreTax=totalCostOfPants+totalCostOfShirts+totalCostOfBelts;
    //Display Total PreTax Cost
         System.out.println("Total PreTax Cost is "+ totalCostPreTax);
    
    //Calculate Total Sales Tax Sum
         totalSalesTax=salesTaxPants+salesTaxShirts+salesTaxBelts;
    //Convert Total Sales Tax Sum into Proper 2-Decimal Form
         String newTotalSalesTax = String.format("%.2f", totalSalesTax);
    
    //Display Total Sales Tax Sum
          System.out.println("The Sum of all Sales Tax on the items purchased is "+ newTotalSalesTax);
    
    //Calculate Total Cost Paid
    CombinedCost=totalCostPreTax+totalSalesTax;
    String  TotalPaid= String.format("%.2f", CombinedCost);
    
    //Display Total Paid for the Clothing with the PA Sales Tax
    System.out.println("The Total Spent on this transaction with a .06 PA Sales Tax is "+ TotalPaid);
    
     }  //end of main method   
} //end of class
    
    
    
    