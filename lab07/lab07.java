//CSE2 Lab 7
//Prof. Brian Chen
//Date: 3/27/2018
//Jason Nerreau
//Lab Objective: Work with methods
//Lab Task: Create randomly generated word components to create a word


import java.util.Random;
import java.util.Scanner;

public class lab07{
    public static void main (String[]args)
    {
      int a = 1;
      Scanner myScanner = new Scanner(System.in);
      boolean result = false;
      do {
      String description = adjective(a);
      String description1 = adjective(a);
      String noun1 = subject(a);
      String action = verb(a);
      String description2 = adjective(a);
      String noun2 = object(a);
      
      System.out.println("The " + description + " " + description1 + " " + noun1 + " " + action + " the " + description2 + " " + noun2);
        
      System.out.print("Would you like to generate another sentence (Yes/No): ");

      while(myScanner.hasNext()){
       String userAnswer=myScanner.next();
      if ((userAnswer.equals("Yes"))){
        result = true; 
        break;
      }
       else if ((userAnswer.equals("No"))){
         result = false;
        break;
       }
       else {
         System.out.print("Incorrect response. Please answer either yes or no on whether you would like to generate another sentence: ");
       }
      } 
      
    } while (result == true);
    }
  
  
  
  
  public static String adjective(int x){
    Random randomGenerator = new Random();
    int randomInt1 = randomGenerator.nextInt(10);
    String answer = "";
   switch (randomInt1){
     case 0:
       answer = "green";
       break;
     case 1: 
       answer = "crazy";
       break;
     case 2: 
       answer = "fresh";
       break;
     case 3: 
       answer = "silly";
       break;
     case 4:
       answer = "awesome";
       break;
     case 5:
       answer = "ugly";
       break;
     case 6:
        answer = "nice";
       break;
     case 7: 
       answer = "polite";
       break;
     case 8: 
       answer = "hungry";
       break;
     case 9:
       answer = "fuzzy";
       break;
   }  
    return answer;
    }
  
  public static String subject(int y){
    Random randomGenerator = new Random();
    int randomInt2 = randomGenerator.nextInt(10);
    String answer = "";
    switch (randomInt2){
      case 0:
       answer = "dog";
        break;
     case 1: 
       answer = "muffin";
        break;
     case 2: 
       answer = "television";
        break;
     case 3: 
       answer = "snowman";
        break;
     case 4:
       answer = "footrest";
        break;
     case 5:
       answer = "kitten";
        break;
     case 6:
        answer = "pizza shop";
        break;
     case 7: 
       answer = "sunfloweer";
        break;
     case 8: 
       answer = "coconut";
        break;
     case 9:
       answer = "golfball";
        break;
    }
    return answer;
    
  }
  
  public static String verb(int z){
    Random randomGenerator = new Random();
    int randomInt3 = randomGenerator.nextInt(10);
    String answer = "";
    switch (randomInt3){
      case 0:
       answer = "jumped";
        break;
     case 1: 
       answer = "ate";
        break;
     case 2: 
       answer = "sneezed";
        break;
     case 3: 
       answer = "melted";
        break;
     case 4:
       answer = "smelt";
        break;
     case 5:
       answer = "kicked";
        break;
     case 6:
        answer = "chopped";
        break;
     case 7: 
       answer = "hugged";
        break;
     case 8: 
       answer = "punched";
        break;
     case 9:
       answer = "watched";
        break;
    }
    return answer;
  }
  
  public static String object(int r){
    Random randomGenerator = new Random();
    int randomInt4 = randomGenerator.nextInt(10);
    String answer = "";
    switch (randomInt4){
      case 0:
       answer = "pumpkin";
        break;
     case 1: 
       answer = "squirrel";
        break;
     case 2: 
       answer = "ogre";
        break;
     case 3: 
       answer = "cheese steak";
        break;
     case 4:
       answer = "sombrero";
        break;
     case 5:
       answer = "burrito";
        break;
     case 6:
        answer = "mitten";
        break;
     case 7: 
       answer = "kangaroo";
        break;
     case 8: 
       answer = "fortune cookie";
        break;
     case 9:
       answer = "stopsign";
        break;
    }
    return answer;
  }
  
  
  
  
  } 
