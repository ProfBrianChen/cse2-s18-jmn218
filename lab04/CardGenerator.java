//CSE2 Lab 4
//Prof. Brian Chen
//Date: 2/16/2018
//Jason Nerreau
//Lab Objective: Use if statements, switch statements  and in using Math.random(), a random number generator.
//Lab Task: Magician practicing tricks and utilizing a random number generator to help them.


// Begin Public Class
public class CardGenerator{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
        int card = (int)(Math.random()*53);
        //Creating the random number generator with variable name card
        
        String suit;
        //Initializing the variable to represent suit
          
        //If-Else statements to determine which random card was created and to give it the corresponding correct card suit                
        if( card <= 13 ){
        suit = "Diamonds";
          } else if( card >= 14 && card <= 26 ){
          suit = "Clubs";
            } else if( card >= 27 && card <=39 ){
            suit = "Hearts";
              } else suit = "Spades";
       
         //Using modulus and a division factor of 13 to return a realistic value for playing cards
         //The modulus returns an integer value for the variable formattedCard
         int formattedCard = card % 13;
         
         //Creating a variable in type String to determine the identity of the Card
         String identityCard;
         
         //Using the switch statement as the determinant of what the identiy of the card will be based on the formattedCard value 
         switch (formattedCard) {
           case 1: identityCard = "Ace";
           break; 
             
             case 2: identityCard = "2";
           break; 
             
             case 3: identityCard = "3";
           break; 
             
             case 4: identityCard = "4";
           break; 
             
             case 5: identityCard = "5";
           break; 
             
             case 6: identityCard = "6";
           break; 
             
             case 7: identityCard = "7";
           break; 
             
             case 8: identityCard = "8";
           break; 
             
             case 9: identityCard = "9";
           break; 
             
             case 10: identityCard = "10";
           break; 
        
           case 11: identityCard = "Jack";
           break;
           
           case 12: identityCard = "Queen";
           break;
           
           //King is case 0 because King is represented by 13 and if it is 'modulo 13', then remainder will be 0 if it is a King number 
           case 0: identityCard = "King";  
           break;
           
           //A switch block requires a default case, however, based on the values created there will never be an invalid card case
           default: identityCard = "invalid card"; 
         }
  //Print out what was chosen and including both the card value and the suit of the card selected 
  System.out.println("You picked the " + identityCard + " of " + suit);
         }  //end of main method   
} //end of class     
                  
         
                           
                          
          