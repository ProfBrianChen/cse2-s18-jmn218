//CSE2 Lab 5
//Prof. Brian Chen
//Date: 3/2/2018
//Jason Nerreau
//Lab Objective: Become familiar with loops, a critical piece of syntax that is essential for many programming languages.
//Lab Task: Print out a simple twist on the screen

import java.util.Scanner;

public class TwistGenerator{
		public static void main(String[] args){
      
  Scanner myScanner = new Scanner(System.in);
      
  System.out.println ("enter integer (positive): ");
  int entry = 0;
	while(entry==0){
	System.out.println ("Enter a positive integer ");
	if(myScanner.hasNextInt()){
    
		int length = myScanner.nextInt();
    
		if (length > 0){
			entry++;
			int xAmount = (length - 1)/3;
			int slashAmount = length;
			int number1 = 0;
			int number2 = 0;
			int number3 = 0;
			while(number1<slashAmount){
				System.out.print("\\ ");
				number1+=2;
				if(number1<slashAmount){
					System.out.print("/");
					number1++;
				}
			}
		System.out.println("");
			while(number2<xAmount){
				System.out.print(" X ");
				number2++;
			}
			System.out.println("");
			while(number3<slashAmount){
				System.out.print("/ ");
				number3+=2;
				if(number3 < slashAmount);{
					System.out.print("\\");
					number3++;
				}
			}
		}
		else{
			System.out.println("Incorrect integer entry. Enter positive integer");
		}
	}
	else{
		System.out.println("Incorrect integer entry. Enter positive integer");
		myScanner.next();
			}
 
		}
	}
  
}
         