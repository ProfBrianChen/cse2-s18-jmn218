import java.util.Scanner;

public class lab09{
    public static void main (String[]args){
     Scanner myScanner = new Scanner(System.in);
      int[] array0 = new int[10];
      System.out.println("The original array contains 10 positions");
      for (int i = 0; i < array0.length; i++){
      System.out.println("Enter the value for the " + i + " spot");
       array0[i] = myScanner.nextInt();
    }
      
      int array1[] = copy(array0);
      print(array0);
      
      System.out.println("The inverter 1 output:");
      int[] array2 = inverter(array1);
      int array3[] = inverter2(array2);
    }
    
  
  public static int[] copy(int[] x){
    int[] newArray = new int[x.length];
    for (int i = 0; i < x.length; i++){
      newArray[i] = x[i];
    }
    return newArray;
  }
  
  
  public static int[] inverter(int[] x){
    int[] tempArray = new int[x.length];
    for (int i = 0; i < x.length; i++){
      tempArray[i] = x[9-i];
    }
    print(tempArray);
    return tempArray;
    
  }
  
  public static int[] inverter2(int[] x){
  int[] tempArray = new int[x.length];
  tempArray = copy(x);
   System.out.println("The inverter 2 output:");
  tempArray = inverter(tempArray);
  return tempArray;  
  }
  
  public static void print(int[] x){
   for (int i = 0; i < x.length; i++){
      System.out.print(x[i] + "    ");
    }
    System.out.println("");
    
  }
  
}